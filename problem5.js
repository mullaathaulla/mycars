function problem3(inventory){
    let arr=[];
    for(let i=0;i<inventory.length;i++){
        arr.push(inventory[i].car_year);
    }
    let finalArr=[];
    for(let i=0;i<arr.length;i++){
        if(arr[i]<2000){
            finalArr.push(arr[i]);
        }
    }
    return finalArr;
}

module.exports=problem3;