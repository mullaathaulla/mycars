function problem2(inventory){
    let ln=inventory.length;
    let ob=inventory[ln-1];
    return `Last car is a ${ob.car_make} ${ob.car_model}`;

}

module.exports=problem2;