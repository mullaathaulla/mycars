function problem6(inventory){
    let BMWAndAudi=[]
    for(let i=0;i<inventory.length;i++){
        if(inventory[i].car_make==='Audi' || inventory[i].car_make==='BMW'){
            BMWAndAudi.push({"id":inventory[i].id,"car_make":inventory[i].car_make,
        "car_model":inventory[i].car_model,"car_year":inventory[i].car_year});
        }
    }
    return BMWAndAudi;

}

module.exports=problem6;
